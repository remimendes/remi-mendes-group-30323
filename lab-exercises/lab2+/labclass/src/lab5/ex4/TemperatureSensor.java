package lab5.ex4;

import lab5.ex3.Sensor;

import java.util.Random;

public class TemperatureSensor extends Sensor {
    public TemperatureSensor(){};
    @Override
    public int readValue() {
        Random r = new Random();
        return r.nextInt(101);
    }
}
