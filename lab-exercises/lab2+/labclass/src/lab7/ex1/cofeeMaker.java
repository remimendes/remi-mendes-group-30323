package lab7.ex1;

class CofeeMaker {
    Cofee makeCofee(){
        System.out.println("Make a coffe");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        try {
            Cofee cofee = new Cofee(t,c);
            return cofee;
        }
        catch (TooManyCofeeException e){
            System.out.println("Exception:"+e.getMessage()+" number of cofee="+e.getNbOfCofee());
        }
        return null;
    }
}
