package lab4;

import lab3.Ex1.Circle;

public class CircleBis {
    private String color="red";
    private double radius;

    public CircleBis(){
    }
    public CircleBis(double radius){
        this.radius=radius;
    }
    public double getRadius(){
        return this.radius;
    }
    public double getArea(){
        return Math.PI*radius*radius;
    }

    @Override
    public String toString() {
        return "CircleBis{" +
                "color='" + color + '\'' +
                ", radius=" + radius +
                '}';
    }

    public static void main(String[] args) {
        Circle c = new Circle(4);
        System.out.print(c.getRadius());
    }
}
