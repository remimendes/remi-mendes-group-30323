package lab10.ex6;

public class ChronometerThread extends Thread{
    private int min=0,hour=0,sec=0,cSec=0;
    private boolean stop=false;
    public ChronometerThread(){
    }

    @Override
    public void run() {
        while (true){
            try {
                sleep(10);
                cSec+=1;
                if (cSec==100){
                    cSec=0;
                    sec+=1;
                }
                if (sec==60){
                    sec=0;
                    min+=1;
                }
                if (min==60){
                    min=0;
                    hour+=1;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(stop){
            synchronized (this){
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            }
        }
    }

    public String getTime(){
        synchronized (this){
            return hour+":"+min+":"+sec+":"+cSec;
        }
    }

    public void reset(){
        synchronized (this){
            hour=0;
            min=0;
            sec=0;
            cSec=0;
        }
    }

    public void setStop(boolean stop) {
        this.stop = stop;
    }

    public void pause(){
        try {
            synchronized (this){
            this.wait();}
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void unpause(){
        this.notify();
    }
}
