package lab10.ex6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChronometerFrame extends JFrame {
    private ChronometerThread time;
    private JButton resetButton;
    private JButton pauseButton;
    private JLabel timeLabel;

    public ChronometerFrame(ChronometerThread time) {
        setTitle("Chronometer");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200, 250);
        setVisible(true);
        this.time = time;
        time.start();
    }

    public void init() {
        this.setLayout(new GridLayout(3, 1));
        timeLabel = new JLabel();
        pauseButton = new JButton("pause");
        resetButton = new JButton("reset");

        pauseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (pauseButton.getText().equals("pause")) {
                    pauseButton.setText("play");
                    time.setStop(true);
                } else {
                    pauseButton.setText("pause");
                    synchronized (time) {
                        time.setStop(false);
                        time.notify();
                    }
                }
            }
        });

        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                time.reset();
            }
        });

        add(timeLabel);
        add(pauseButton);
        add(resetButton);

    }

    public void setTime() {
        timeLabel.setText(time.getTime());
    }


}
