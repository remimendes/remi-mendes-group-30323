package edu.utcn.lab12.vehicles;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ElectricBatteryTest {

    /**
     * Expect battery to throw exception if charged more than 100%
     */
    @Test
    public void charge() {
    	System.out.println(BatteryException.class);
        ElectricBattery bat = new ElectricBattery();
        
        for(int i=0;i<110;i++)
        	try {
				bat.charge();
			} catch (Exception e) {
				System.out.println("Caught the exception : " + e.getMessage());
				assertTrue(BatteryException.class == e.getClass());
			}
			
			

    }
}