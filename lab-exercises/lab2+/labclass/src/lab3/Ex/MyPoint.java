package lab3.Ex1;

import static java.lang.Math.sqrt;

public class MyPoint {
    private int x;
    private int y;

    public MyPoint(){
        x=0;
        y=0;
    }

    public MyPoint(int x,int y){
        this.x=x;
        this.y=y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString(){
        return "("+x+","+y+")";
    }

    public double distance(int x,int y){
        return Math.sqrt(Math.pow(x - this.x,2)+Math.pow(y - this.y,2));
    }

    public double distance(MyPoint another){
        return Math.sqrt(Math.pow(another.getX() - this.x,2)+Math.pow(another.getY() - this.y,2));
    }

    public static void main(String[] args) {
        MyPoint p1 = new MyPoint();
        MyPoint p2 = new MyPoint(1,1);
        p2.setY(3);
        System.out.println(p1.toString());
        System.out.println(p2.getY());
        System.out.println(p1.distance(1,3));
        System.out.println(p1.distance(p2));
    }
}
