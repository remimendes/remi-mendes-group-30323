package lab11.ex2;

import javax.swing.*;
import java.awt.*;
import java.util.Iterator;

public class ViewAll extends JFrame {
    public ViewAll(Controller controller) {
        setTitle("All the stocks");
        setSize(350, 350);
        JLabel label = new JLabel();
        setLayout(new GridLayout());
        label.setText(controller.getAllProducts());
        add(label);
        setVisible(true);
    }
}