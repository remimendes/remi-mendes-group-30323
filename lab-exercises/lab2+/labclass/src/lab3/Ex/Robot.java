package lab3.Ex1;

public class Robot {
    private int x;

    public Robot(){
        this.x=1;
    }

    public void change(int k){
        if (k>0){
            this.x+=k;
        }
    }
    @Override
    public String toString(){
        return String.valueOf(this.x);
    }

    public static void main(String[] args) {
        Robot r = new Robot();
        r.change(7);
        System.out.print(r.toString());
        r.toString();
    }
}

