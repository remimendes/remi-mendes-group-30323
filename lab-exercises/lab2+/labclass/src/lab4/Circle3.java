package lab4;

import static java.lang.Math.pow;

public class Circle3 extends Shape{
    double radius =1;

    public Circle3(){
        super();
    }
    public Circle3(double radius){
        super();
        this.radius=radius;
    }
    public Circle3(double radius,String color,boolean filled){
        super(color,filled);
        this.radius=radius;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public void setColor(String color) {
        super.setColor(color);
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(){
        return Math.PI*pow(getRadius(),2);
    }

    public double getPerimeter(){
        return Math.PI*2*getRadius();
    }
    @Override
    public String toString(){
        return "A circle with radius = "+radius+" ,which is a subclass of "+super.toString();
    }
}
