package lab9.ex4;

import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        HashMap<String, Button> ButtonMap = new HashMap<>();
        Frame f=new Frame(400,400);
        Table t=new Table(ButtonMap,f);
        for (int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                ButtonMap.put(Character.toString((char)(i))+Character.toString((char)(j)),new Button(i,j,f,t));
            }
        }
        f.setVisible(true);
        while (!t.isFinish()){
        }
    }
}
