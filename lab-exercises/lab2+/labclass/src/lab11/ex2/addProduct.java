package lab11.ex2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class addProduct extends JFrame {
    private Controller controller;
    public addProduct(Controller controller){
        this.controller=controller;
        setTitle("ex2 lab11");
        setSize(350,350);
        setLayout(new GridLayout(4,1));
        init();
        setVisible(true);
    }

    public void init(){
        TextField name=new TextField("name");
        TextField price=new TextField("price");
        TextField quantity=new TextField("quantity");
        JButton add=new JButton("add product");

        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.addProduct(name.getText(), price.getText(), quantity.getText());
                name.setText("name");
                price.setText("price");
                quantity.setText("quantity");
            }
        });

        add(name);
        add(price);
        add(quantity);
        add(add);
        setVisible(true);
    }
}
