package lab11.ex2;

import java.awt.*;
import java.util.Iterator;

public class Controller {
    StockModel model;

    public Controller(StockModel model) {
        this.model = model;
    }

    public String getAllProducts() {
        String text = "";
        String name;
        Iterator<String> i = model.getAllKeys().iterator();
        while (i.hasNext()) {
            name = i.next();
            text += "<html> name :" + name + " price :" + model.getPrice(name) + " quantity :" + model.getQuantity(name) + "<br>";
        }
        return text;
    }

    public void addProduct(String name, String price, String quantity) {
        model.addProduct(name, Float.parseFloat(price), Integer.parseInt(quantity));
    }

    public void deleteProduct(String name) {
        model.deleteProduct(name);
    }

    public void changeStock(String name, String newQuantity) {
        model.getStock(name).setQuantity(Integer.parseInt(newQuantity));
    }

    public void createView() {
        new StockView(this);
    }

    public void createAddView() {
        new addProduct(this);
    }

    public void createChangeView() {
        new changeStock(this);
    }

    public void deleteView() {
        new deleteProduct(this);
    }

    public void viewAllView() {
        new ViewAll(this);
    }

    public static void main(String[] args) {
        StockModel m = new StockModel();
        Controller c = new Controller(m);
        c.createView();
    }
}
