package lab9.ex4;

import javax.swing.*;

class Frame extends JFrame{
    private JTextArea text;
    public Frame(int width,int height){
        setTitle("TIC TAC TOE");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(width,height);
        this.setLayout(null);
        init();

    }

    public void init(){
        text = new JTextArea();
        text.setBounds(0,this.getHeight()-60,this.getWidth(),20);
        add(text);
    }


    public void setText(String text){
        this.text.setText(text);
    }
}
