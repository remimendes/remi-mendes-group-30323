package lab11.ex2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class deleteProduct extends JFrame {
    private Controller controller;

    public deleteProduct(Controller controller) {
        this.controller = controller;
        setTitle("ex2 lab11");
        setSize(350, 350);
        setLayout(new GridLayout(2, 1));
        init();
        setVisible(true);
    }

    public void init() {
        TextField name = new TextField("name");
        JButton add = new JButton("delete product");

        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.deleteProduct(name.getText());
                name.setText("name");
            }
        });

        add(name);
        add(add);
    }
}
