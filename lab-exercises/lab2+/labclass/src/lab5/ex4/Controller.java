package lab5.ex4;


public class Controller {
    private LightSensor lightSensor = new LightSensor();
    private TemperatureSensor temperatureSensor = new TemperatureSensor();

    private static Controller single_instance = null;

    public Controller() {//the constructor should be private
    }

    public static Controller getInstance()
    {
        if (single_instance == null)
            single_instance = new Controller();

        return single_instance;
    }

    public void control() throws InterruptedException {
        for (int i = 0; i < 20; i++) {
            System.out.println("light sensor = " + lightSensor.readValue());
            System.out.println("temperature sensor = " + temperatureSensor.readValue());
            Thread.sleep(1000);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Controller c = getInstance();
        c.control();
    }
}
