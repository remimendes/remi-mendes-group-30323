package lab7.ex1;

class Cofee {
    private static int nbOfCofee = 0;
    private int temp;
    private int conc;

    Cofee(int t, int c) throws TooManyCofeeException {
        nbOfCofee+=1;;
        if (nbOfCofee>2){
            throw new TooManyCofeeException(nbOfCofee,"Too many Cofee !");
        }
        temp = t;
        conc = c;
    }

    int getTemp() {
        return temp;
    }

    int getConc() {
        return conc;
    }

    @Override
    public String toString() {
        return "[cofee temperature=" + temp + ":concentration=" + conc + "]";
    }
}
