package lab5.ex1;

public class MainEx1 {
    public static void main(String[] args) {
        Circle c1=new Circle(3,"green",false);
        Rectangle r1 = new Rectangle(5,9,"black",true);
        Square s1=new Square();
        System.out.println(c1);
        System.out.println(r1);
        System.out.println(s1);
        s1.setSide(98);
        System.out.println(s1);
        System.out.println(r1.getArea());
        System.out.println(c1.getPerimeter());
        System.out.println(c1.getRadius());
    }
}
