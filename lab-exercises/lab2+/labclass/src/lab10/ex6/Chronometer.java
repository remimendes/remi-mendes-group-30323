package lab10.ex6;

public class Chronometer extends Thread{
    private ChronometerThread t=new ChronometerThread();
    private ChronometerFrame f =new ChronometerFrame(t);
    public Chronometer(){
    }

    @Override
    public void run() {
        while (true){
            f.setTime();
        }
    }

    public static void main(String[] args) {
        Chronometer c =new Chronometer();
        c.start();
    }
}
