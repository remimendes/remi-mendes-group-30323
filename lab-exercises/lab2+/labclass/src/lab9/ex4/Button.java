package lab9.ex4;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Button extends JFrame {
    private JButton button;
    private int x;
    private int y;
    public Button(int x, int y,JFrame frame,Table table){
        button=new JButton(" ");
        this.x=x;
        this.y=y;
        button.setBounds(x*((frame.getWidth()-20)/4+10)+10,y*((frame.getHeight()-60)/4+10)+10,(frame.getSize().width-20)/4,(frame.getSize().height-60)/4);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                table.play(Character.toString((char)(x))+Character.toString((char)(y)));
            }
        });
        frame.add(button);
    }

    public void changeText(String text){
        button.setText(text);
    }
    public String getText(){
        return button.getText();
    }
}
