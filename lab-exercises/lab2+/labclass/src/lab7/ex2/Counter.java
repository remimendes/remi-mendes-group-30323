package lab7.ex2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Counter {
    private File file;
    private Scanner sc;
    public Counter(String filePath) throws FileNotFoundException {
        file = new File(filePath);
        sc = new Scanner(file);
        sc.useDelimiter("");
    }

    public int CountChar(char c){
        int nbOfChar=0;
        while (sc.hasNext()){
            if (sc.next().charAt(0)==c){
                nbOfChar+=1;
            }
        }
        return nbOfChar;
    }
}
