package lab6.ex2;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String owner,double balance){
        this.owner=owner;
        this.balance=balance;
    }

    public void deposit(double amount){
        balance+=amount;
    }

    public void withdraw(double amount){
        balance-=amount;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }

    public String getOwner() {
        return owner;
    }
}