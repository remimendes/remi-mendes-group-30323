package lab9.ex3;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileButton extends JFrame implements ActionListener{
    JTextField file;
    JTextArea text;
    public FileButton(){
        setTitle("ex3 lab9");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500,500);
        init();
        this.setLayout(null);
        setVisible(true);
    }

    public void init(){
        file = new JTextField("here the path of your file");
        text = new JTextArea();
        JButton B = new JButton("read file");
        B.setBounds(0,50,100,50);
        B.addActionListener(this::actionPerformed);
        B.setActionCommand("read");
        text.setBounds(0,120,500,300);
        file.setBounds(0,0,200, 50);
        add(file);
        add(text);
        add(B);
    }
    @Override
    public void actionPerformed(ActionEvent e){
        int tmp;
        String tmpS="";
        if(e.getActionCommand().equals("read")){
            try {
                FileReader f=new FileReader(file.getText());
                while ((tmp=f.read())!=-1){
                    tmpS+=(char)tmp;
                }
                text.setText(tmpS);
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        FileButton b=new FileButton();
    }
}
