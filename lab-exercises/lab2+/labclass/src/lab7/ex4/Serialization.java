package lab7.ex4;

import java.io.*;

public class Serialization {
    public Serialization(){

    }

    public void ser(String path,Car car1) throws IOException {
        FileOutputStream file = new FileOutputStream(path);
        ObjectOutputStream out = new ObjectOutputStream(file);

        out.writeObject(car1);

        out.close();
        file.close();

    }

    public Car deSer(String path) throws IOException, ClassNotFoundException {
        FileInputStream file = new FileInputStream(path);
        ObjectInputStream in = new ObjectInputStream(file);

        // Method for deserialization of object
        Car car1 = (Car)in.readObject();

        in.close();
        file.close();

        return car1;
    }
}
