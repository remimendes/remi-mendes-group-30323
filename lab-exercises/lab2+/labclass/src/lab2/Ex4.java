package lab2;

import java.util.Scanner;

public class Ex4 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.print("how many numbers in the vector ? :");
        int n=in.nextInt();
        int list[] = new int[n];
        int max;
        for (int i=0;i<n;i++){
            System.out.print("int number "+i+" :");
            list[i]=in.nextInt();
        }
        max=list[0];
        for(int i=1;i<n;i++){
            max=max<list[i]?list[i]:max;
        }
        System.out.print("max :"+max);
    }
}
