package lab5.ex3;

public class Controller {
    private LightSensor lightSensor = new LightSensor();
    private TemperatureSensor temperatureSensor = new TemperatureSensor();

    public Controller() {
    }

    public void control() throws InterruptedException {
        for(int i=0;i<20;i++){
            System.out.println("light sensor = "+lightSensor.readValue());
            System.out.println("temperature sensor = "+temperatureSensor.readValue());
            Thread.sleep(1000);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Controller c =new Controller();
        c.control();
    }
}
