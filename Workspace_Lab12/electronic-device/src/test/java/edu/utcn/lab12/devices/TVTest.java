package edu.utcn.lab12.devices;

import static org.junit.Assert.assertTrue;

public class TVTest {
	public static void main(String[] args) {
		
		TV t = new TV(5);
		t.channelUp();
		assertTrue(t.getChannel() == 6);
		
		for (int i=0; i<4; i++)
			t.channelDown();
		assertTrue(t.getChannel() == 2);
	}
}
