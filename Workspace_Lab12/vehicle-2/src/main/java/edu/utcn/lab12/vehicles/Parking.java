package edu.utcn.lab12.vehicles;

import java.util.ArrayList;

public class Parking {

    public ArrayList<Vehicle> parkedVehicles = new ArrayList<>();

    public void parkVehicle(Vehicle e){
    	parkedVehicles.add(e);
    }

    /**
     * Sort vehicles by length. Bubble sort
     */
    public void sortByWeight(){
    	boolean isSorted = false;
    	while(!isSorted) {
    		isSorted = true;
    		for(int i=0; i<parkedVehicles.size()-1; i++) {
    			if(parkedVehicles.get(i).getWeight() > parkedVehicles.get(i+1).getWeight()){
    				Vehicle temp = parkedVehicles.get(i);
    				parkedVehicles.set(i, parkedVehicles.get(i+1));
    				parkedVehicles.set(i+1, temp);
    				isSorted = false;
    			}
    		}
    	}
    }

    public Vehicle get(int index){
        return parkedVehicles.get(index);
    }

}
