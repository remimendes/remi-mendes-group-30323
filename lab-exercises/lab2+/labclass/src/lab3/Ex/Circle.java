package lab3.Ex1;

public class Circle {
    private String color="red";
    private int radius;

    public Circle(int radius){
        this.radius=radius;
    }
    public Circle(int radius, String color){
        this.radius=radius;
        this.color=color;
    }
    public int getRadius(){
        return this.radius;
    }
    public double getArea(){
        return Math.PI*radius*radius;
    }

    public static void main(String[] args) {
        Circle c = new Circle(4);
        System.out.print(c.getRadius());
    }
}
