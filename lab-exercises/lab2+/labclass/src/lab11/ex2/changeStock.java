package lab11.ex2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class changeStock extends JFrame {
    private Controller controller;

    public changeStock(Controller controller) {
        this.controller = controller;
        setTitle("ex2 lab11");
        setSize(350, 350);
        setLayout(new GridLayout(3, 1));
        init();
        setVisible(true);
    }
    public void init(){
        TextField name=new TextField("name");
        TextField quantity=new TextField("quantity");
        JButton add=new JButton("change quantity");

        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.changeStock(name.getText(), quantity.getText());
                name.setText("name");
                quantity.setText("new quantity");
            }
        });

        add(name);
        add(quantity);
        add(add);
        setVisible(true);
    }
}
