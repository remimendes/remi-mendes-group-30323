package lab5.ex2;

import lab5.ex2.Image;

public class RotatedImage implements Image {

    private String fileName;

    public RotatedImage(String fileName){
        this.fileName = fileName;
        loadFromDisk(fileName);
    }

    @Override
    public void display() {
        System.out.println("Display rotated " + fileName);
    }

    private void loadFromDisk(String fileName){
        System.out.println("Loading " + fileName);
    }
}