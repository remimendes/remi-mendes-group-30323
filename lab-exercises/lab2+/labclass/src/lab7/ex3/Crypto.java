package lab7.ex3;

import java.io.*;
import java.util.Scanner;


public class Crypto {
    public Crypto() {

    }

    public void encrypt(String filePath) throws IOException {
        FileReader fr = new FileReader(filePath);
        filePath = filePath.substring(0, filePath.length() - 4);
        File fileEnc = new File(filePath + ".enc");
        fileEnc.createNewFile();
        FileWriter fWriter = new FileWriter(filePath + ".enc");
        int i=0;
        int a=0;
        while ((a=fr.read())!=-1){
            i++;
            fWriter.write((char)(a+1));
        }
        fWriter.close();
    }

    public void decrytp(String filePath) throws IOException {
        FileReader fr = new FileReader(filePath);
        filePath = filePath.substring(0, filePath.length() - 4);
        File fileEnc = new File(filePath + ".dec");
        fileEnc.createNewFile();
        FileWriter fWriter = new FileWriter(filePath + ".dec");
        int i=0;
        int a=0;
        while ((a=fr.read())!=-1){
            i++;
            fWriter.write((char)(a-1));
        }
        fWriter.close();
    }

    public static void main(String[] args) throws IOException {
        Crypto c = new Crypto();
        Scanner sc = new Scanner(System.in);
        String tmp;
        Boolean b=true;
        while(b){
            System.out.print("Do you want to encrypt or decrypt ?-->");
            tmp= sc.nextLine();
            if(tmp.equals("encrypt")){
                System.out.print("what is the path ?-->");
                tmp=sc.nextLine();
                c.encrypt(tmp);
            }
            else if (tmp.equals("decrypt")){
                System.out.print("what is the path ?-->");
                tmp=sc.nextLine();
                c.decrytp(tmp);
            }

            System.out.print("do you want to continue ? y/n -->");
            if (sc.nextLine().equals("n")){
                b=false;
            }
        }
    }
}
