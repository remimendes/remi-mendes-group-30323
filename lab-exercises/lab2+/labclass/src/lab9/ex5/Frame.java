package lab9.ex5;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

class Frame extends JFrame {
    private ArrayList<Controler> StationList;
    private JTextArea text;
    private JButton nextButton;
    private boolean next=false;
    private ArrayList<JTextArea> TextAreaList=new ArrayList<>();
    private int freeTextAreaIndex=0;
    public Frame(int width,int height,int nbOfTrainStation,ArrayList<Controler> StationList){
        this.StationList=StationList;
        setTitle("ex5");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(width,height);
        init(nbOfTrainStation);
        this.setLayout(null);
        this.setVisible(true);
    }

    public void init(int nbOfTrainStation){
        text = new JTextArea();
        nextButton=new JButton("next step");
        text.setBounds(0,this.getHeight()/2-400,this.getWidth(),300);
        nextButton.setBounds(0,500,100,30);
        nextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setNext(true);
            }
        });
        add(text);
        add(nextButton);
        for (int i=0;i<nbOfTrainStation;i++){
            TextAreaList.add(new JTextArea());
            System.out.println(i*(getWidth()/nbOfTrainStation));
            TextAreaList.get(i).setBounds(i*(getWidth()/nbOfTrainStation),0,(this.getWidth()/nbOfTrainStation),200);
            add(TextAreaList.get(i));
        }

        JTextField TrainName=new JTextField("name of the train");
        JTextField SegmentId=new JTextField("Id of the segment");
        JTextField Destination=new JTextField("destination of the train");

        TrainName.setBounds(0,530,300,50);
        SegmentId.setBounds(0,580,300,50);
        Destination.setBounds(0,620,300,50);

        add(TrainName);
        add(SegmentId);
        add(Destination);

        JButton AddTrain=new JButton("add train");
        AddTrain.setBounds(300,530,100,50);
        add(AddTrain);
        AddTrain.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for(Controler controler:StationList){
                    for(Segment segment:controler.list){
                        try{
                        if( segment.id==Integer.valueOf(SegmentId.getText())){
                            controler.arriveTrain(new Train(Destination.getText(),TrainName.getText()),segment.id);
                            TrainName.setText("name of the train");
                            SegmentId.setText("Id of the segment");
                            Destination.setText("destination of the train");
                            text.setText("");
                            for (Controler controler1:StationList){
                                controler1.displayStationState2();
                            }
                            break;
                        }}
                        catch (Exception exception){}
                    }
                }
            }
        });
    }

    public void addText(String text){
        this.text.append(text);
    }

    public void clearText(){
        this.text.setText("");
    }

    public boolean isNext() {
        return next;
    }

    public void setNext(boolean next) {
        this.next = next;
    }

    public int getFreeTextArea(){
        freeTextAreaIndex++;
        return freeTextAreaIndex-1;
    }

    public void addTextArea(String text,int i){
        TextAreaList.get(i).append(text);
    }

    public void clearTextArea(int i){
        TextAreaList.get(i).setText("");
    }
}

