package lab4;

public class Rectangle extends Shape{
    public double width = 1;
    public double length =1;

    public Rectangle(){
        super();
    }
    public Rectangle(double width, double length){
        super();
        this.width=width;
        this.length=length;
    }
    public Rectangle(double width,double length,String color,boolean filled){
        super();
        this.width=width;
        this.length=length;
        setColor(color);
        setFilled(filled);
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea(){
        return width*length;
    }

    public double getPerimeter(){
        return width*2+2*length;
    }

    @Override
    public String toString(){
        return "A Rectangle with width = "+width+" and length = "+length+super.toString();
    }
}
