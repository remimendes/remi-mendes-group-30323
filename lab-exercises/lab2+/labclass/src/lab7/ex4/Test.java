package lab7.ex4;

import java.io.IOException;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Scanner sc = new Scanner(System.in);
        Serialization s=new Serialization();
        int price;
        String model;
        String tmp;
        while (true){
            System.out.println("Do you want to save a new car objects on a local folder or read a car object from a local folder ? write/read");
            tmp=sc.nextLine();
            if(tmp.equals("write")){
                System.out.println("what is the model ?");
                model= sc.nextLine();
                System.out.println("what is the price ?");
                price = sc.nextInt();
                sc.nextLine();
                System.out.println("what is the path ?");
                s.ser(sc.nextLine(),new Car(model,price));
            }
            else if (tmp.equals("read")){
                System.out.println("what is the path ?");
                System.out.println(s.deSer(sc.nextLine()));
            }

            System.out.println("do you want to stop ? y/n");
            if (sc.nextLine().equals("y")){
                break;
            }
        }
    }
}
