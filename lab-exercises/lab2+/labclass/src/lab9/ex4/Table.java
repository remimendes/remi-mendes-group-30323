package lab9.ex4;

import javax.swing.*;
import java.util.HashMap;

public class Table {
    private HashMap<String,Button> hashMap;
    private boolean turn=false;
    private Frame frame;

    public Table(HashMap<String,Button> hashMap,Frame frame){
        this.hashMap=hashMap;
        this.frame=frame;
    }

    public void play(String buttonId){
        if (turn){
            hashMap.get(buttonId).changeText("X");
            turn=false;
        }
        else{
            hashMap.get(buttonId).changeText("O");
            turn=true;
        }
    }

    public boolean isFinish(){
        int nb=0;
        int nb2=0;
        for (int i=0;i<3;i++){
            for (int j = 0; j < 3; j++) {
                if (hashMap.get(Character.toString((char)(i))+Character.toString((char)(j))).getText().equals("X")){
                    nb+=1;
                }
                else if (hashMap.get(Character.toString((char)(i))+Character.toString((char)(j))).getText().equals("O")){
                    nb-=1;
                }
                if (hashMap.get(Character.toString((char)(j))+Character.toString((char)(i))).getText().equals("X")){
                    nb2+=1;
                }
                else if (hashMap.get(Character.toString((char)(j))+Character.toString((char)(i))).getText().equals("O")){
                    nb2-=1;
                }
            }
            if (nb==3 || nb2==3){
                frame.setText("X win");
                return true;
            }
            else if(nb==-3 || nb2==-3){
                frame.setText("O win");
                return true;
            }
            nb=0;
            nb2=0;
        }
        for (int i=0;i<3;i++){
            if (hashMap.get(Character.toString((char)(i))+Character.toString((char)(i))).getText().equals("X")){
                nb+=1;
            }
            else if (hashMap.get(Character.toString((char)(i))+Character.toString((char)(i))).getText().equals("O")){
                nb-=1;
            }

            if (hashMap.get(Character.toString((char)(i))+Character.toString((char)(2-i))).getText().equals("X")){
                nb2+=1;
            }
            else if (hashMap.get(Character.toString((char)(i))+Character.toString((char)(2-i))).getText().equals("O")){
                nb2-=1;
            }
        }
        if (nb==3 || nb2==3){
            frame.setText("X win");
            return true;
        }
        else if(nb==-3 || nb2==-3){
            frame.setText("O win");
            return true;
        }
        return false;
    }
}
