package lab6.ex2;

import java.util.*;

public class Bank {
    private ArrayList<BankAccount> bankAccountList = new ArrayList();

    public Bank() {
    }

    ;

    public void addAccount(String owner, double balance) {
        bankAccountList.add(new BankAccount(owner, balance));
    }

    public void printAccounts() {
        Comparator<BankAccount> C = new Comparator<BankAccount>() {
            @Override
            public int compare(BankAccount b1, BankAccount b2) {
                if (b1.getBalance() > b2.getBalance()) {
                    return -1;
                } else if (b1.getBalance() < b2.getBalance()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        };
        bankAccountList.sort(C);
        for (int i = 0; i < bankAccountList.size(); i++) {
            System.out.println(bankAccountList.get(i));
        }


    }

    public void printAccounts(double minBalance, double maxBalance) {
        Comparator<BankAccount> C = new Comparator<BankAccount>() {
            @Override
            public int compare(BankAccount b1, BankAccount b2) {
                if (b1.getBalance() > b2.getBalance()) {
                    return -1;
                } else if (b1.getBalance() < b2.getBalance()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        };
        bankAccountList.sort(C);
        for (int i = 0; i < bankAccountList.size(); i++) {
            if (bankAccountList.get(i).getBalance() >= minBalance && bankAccountList.get(i).getBalance() <= maxBalance) {
                System.out.println(bankAccountList.get(i));
            }
        }
    }

    public BankAccount getAccount(String owner) {
        int i = 0;
        for (i = 0; i < bankAccountList.size(); i++) {
            if (bankAccountList.get(i).getOwner().equals(owner)) {
                break;
            }
        }
        return bankAccountList.get(i);
    }

    public ArrayList<BankAccount> getAllAccount(){
        Comparator<BankAccount> C = new Comparator<BankAccount>(){
            @Override
            public int compare(BankAccount b1, BankAccount b2) {
                return  b1.getOwner().compareTo(b2.getOwner());
            }
    };
        bankAccountList.sort(C);
        return bankAccountList;
    }


    public static void main(String[] args) {
        Bank b = new Bank();
        b.addAccount("remi", 200);
        b.addAccount("maffffft", 2);
        b.addAccount("val", 400);
        b.addAccount("aaa", 400);
        for (int i=0;i<b.getAllAccount().size();i++){
            System.out.println(b.getAllAccount().get(i));
        };
    }
}

