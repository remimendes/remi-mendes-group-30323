package lab9.ex5;

import javax.swing.*;
import java.util.ArrayList;

public class Controler{

    String stationName;

    ArrayList <Controler> neighbourControllers=new ArrayList<Controler>();

    //storing station train track segments
    ArrayList<Segment> list = new ArrayList<Segment>();
    Frame f;
    private int textIndex;

    public Controler(String gara, Frame f, int textIndex) {
        stationName = gara;
        this.f=f;
        this.textIndex=textIndex;
    }

    void addNeighbourController(Controler v){
        neighbourControllers.add(v);
    }

    void addControlledSegment(Segment s){
        list.add(s);
    }

    /**
     * Check controlled segments and return the id of the first free segment or -1 in case there is no free segment in this station
     *
     * @return
     */
    int getFreeSegmentId(){
        for(Segment s:list){
            if(s.hasTrain()==false)
                return s.id;
        }
        return -1;
    }

    void controlStep(){
        //check which train must be sent

        for(Segment segment:list){
            if(segment.hasTrain()){
                Train t = segment.getTrain();
            for(Controler c:neighbourControllers){
                if(t.getDestination().equals(c.stationName)){
                    //check if there is a free segment
                    int id = c.getFreeSegmentId();
                    if(id==-1){
                        f.addTextArea("Trenul +"+t.name+"din gara "+stationName+" nu poate fi trimis catre "+c.stationName+". Nici un segment disponibil!\n",textIndex);
                        return;
                    }
                    //send train
                    f.addTextArea("Trenul "+t.name+" pleaca din gara "+stationName +" spre gara "+c.stationName+"\n",textIndex);
                    segment.departTRain();
                     c.arriveTrain(t,id);
                }
            }

            }
        }//.for

    }//.


    public void arriveTrain(Train t, int idSegment){
        for(Segment segment:list){
            //search id segment and add train on it
            if(segment.id == idSegment)
                if(segment.hasTrain()==true){
                    f.addTextArea("CRASH! Train "+t.name+" colided with "+segment.getTrain().name+" on segment "+segment.id+" in station "+stationName+"\n",textIndex);
                    return;
                }else{
                    f.addTextArea("Train "+t.name+" arrived on segment "+segment.id+" in station "+stationName+"\n",textIndex);
                    segment.arriveTrain(t);
                    return;
                }
        }

        //this should not happen
        f.addTextArea("Train "+t.name+" cannot be received "+stationName+". Check controller logic algorithm!\n",textIndex);
    }


    public void displayStationState(){
        f.addTextArea("=== STATION "+stationName+" ===\n",textIndex);
        for(Segment s:list){
            if(s.hasTrain())
                f.addText("|----------ID="+s.id+"__Train="+s.getTrain().name+" to "+s.getTrain().destination+"__----------|\n");
            else
                f.addText("|----------ID="+s.id+"__Train=______ catre ________----------|\n");
        }
    }
    public void displayStationState2(){
        for(Segment s:list){
            if(s.hasTrain())
                f.addText("|----------ID="+s.id+"__Train="+s.getTrain().name+" to "+s.getTrain().destination+"__----------|\n");
            else
                f.addText("|----------ID="+s.id+"__Train=______ catre ________----------|\n");
        }
    }

    public void clearTextAreaC(){
        f.clearTextArea(textIndex);
    }
}
