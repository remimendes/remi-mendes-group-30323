package lab9.ex2;

import lab9.ex1.ButtonAndTextField2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CounterAndButton extends JFrame implements ActionListener {
    private int i=0;
    JLabel text;
    public CounterAndButton(){
        setTitle("ex2 lab9");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(350,350);
        setVisible(true);
        init();
    }

    public void init(){
        JButton B = new JButton("add 1");
        B.setBounds(10,10,100, 100);
        B.addActionListener(this::actionPerformed);
        B.setActionCommand("a");
        add(B);
        text = new JLabel("i = "+this.i);
        text.setBounds(10, 100,100, 100);
        add(text);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand()=="a"){
            i++;
            text.setText("i = "+i);
        }
    }

    public static void main(String[] args) {
        CounterAndButton c = new CounterAndButton();
    }
}
