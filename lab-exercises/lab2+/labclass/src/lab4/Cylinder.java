package lab4;

import static java.lang.Math.pow;

public class Cylinder extends CircleBis {
    private double height =1;

    public Cylinder(){
        super();
    }

    public Cylinder(double radius){
        super(radius);
    }

    public Cylinder(double radius, double height){
        super(radius);
        this.height=height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume(){
        return Math.PI*pow(getRadius(),2)*height;
    }
    @Override
    public double getArea(){
        return 2*Math.PI*getRadius()*height;
    }

    public static void main(String[] args) {
        Cylinder c1 = new Cylinder(7,5);
        System.out.println(c1.getVolume());
    }
}
