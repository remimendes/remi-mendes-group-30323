package edu.utcn.lab12.bankaccount;

import java.util.ArrayList;

public class AccountsManager {

	ArrayList<BankAccount> listAccounts = new ArrayList<>();
	
    public void addAccount(BankAccount account){
    	listAccounts.add(account);
    }

    public boolean exists(String id){
        for(BankAccount ba : listAccounts) {
        	if(ba.getId() == id) {
        		return true;
        	}
        }
        return false;
    }

    public int count(){
        return listAccounts.size();
    }
}
