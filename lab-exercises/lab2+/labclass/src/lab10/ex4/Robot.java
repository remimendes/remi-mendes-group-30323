package lab10.ex4;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class Robot extends Thread{
    private int y=ThreadLocalRandom.current().nextInt(0, 100 + 1),x=ThreadLocalRandom.current().nextInt(0, 100 + 1),ID;
    private ArrayList<Robot> RobotList;
    private boolean exit=false;
    public Robot(int id, ArrayList RobotList){
        this.ID=id;
        this.RobotList=RobotList;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public int getID() {
        return ID;
    }

    @Override
    public void run() {
        super.run();
        for (int i = 0; i < 350; i++) {
            this.move();
            this.CheckCollision();
            try {
                this.sleep(1);
            } catch (InterruptedException e) {
               e.printStackTrace();
            }
            if(exit){
                break;
            }
        }
    }

    public void CheckCollision(){
        synchronized (RobotList){
        for (Robot robot:RobotList) {
            if (this.getX()==robot.getX() && this.getY()== robot.getY() && this.getID()!=robot.getID()){
                System.out.println("collision between robot : "+this.getID()+" and robot : "+robot.getID());
                RobotList.remove(robot);
                RobotList.remove(this);
                exit=true;
                break;
            }
        }
        }
    }

    public void move(){
        int ytmp=y;
        int xtmp=x;
        y= ytmp+ThreadLocalRandom.current().nextInt(-1, 1 + 1);
        x= xtmp+ThreadLocalRandom.current().nextInt(-1, 1 + 1);
        if (x>100){
            x--;
        }
        else if (x<0){
            x++;
        }
        if (y>100){
            y--;
        }
        else if (y<0){
            y++;
        }
    }

    public static void main(String[] args) {
        ArrayList<Robot> list=new ArrayList<>();
        System.out.println("simulating 350 movement :");
        for (int i = 0; i < 10; i++) {
            list.add(new Robot(i,list));
        }
        for (int i = 0; i < 10; i++) {
            list.get(i).start();
        }
    }
}
