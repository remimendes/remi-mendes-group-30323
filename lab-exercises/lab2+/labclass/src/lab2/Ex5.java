package lab2;

import java.util.Random;
import java.util.Scanner;

public class Ex5 {
    public static void main(String[] args){
        Random r =new Random();
        Scanner in = new Scanner(System.in);
        System.out.print("how many number do you want to generate ? :");
        int list[]= new int[in.nextInt()];
        boolean sorted;
        int tmp;
        for(int i=0;i< list.length;i++){
            list[i]=r.nextInt(10000);
        }
        for(int i= list.length-1;i>0;i--){
            sorted=true;
            for(int j=0;j<i;j++){
                if (list[j+1]<list[j]){
                    tmp=list[j+1];
                    list[j+1]=list[j];
                    list[j]=tmp;
                    sorted=false;
                }
            }
            if (sorted){
                break;
            }
        }
        for(int n : list){
            System.out.print(n+"\n");
        }
    }
}
