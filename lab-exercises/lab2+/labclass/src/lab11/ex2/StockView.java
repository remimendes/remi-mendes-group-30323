package lab11.ex2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

public class StockView extends JFrame{
    Controller controller;
    public StockView(Controller controller){
        this.controller=controller;
        setTitle("ex2 lab11");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(350,350);
        setLayout(new GridLayout(2,2));
        init();
        setVisible(true);
    }

    public void init(){
        JButton b1 = new JButton("View all the products");
        JButton b2 = new JButton("add new product");
        JButton b3 = new JButton("delete product");
        JButton b4 = new JButton("change product available quantity");
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.viewAllView();
            }
        });
        b2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.createAddView();
            }
        });

        b3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               controller.deleteView();
            }
        });

        b4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.createChangeView();
            }
        });
        add(b1);
        add(b2);
        add(b3);
        add(b4);
    }

}
