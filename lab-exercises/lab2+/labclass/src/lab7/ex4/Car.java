package lab7.ex4;

public class Car implements java.io.Serializable{
    private String model;
    private int price;
    public Car(String model,int price){
        this.model=model;
        this.price=price;
    }
    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", price=" + price +
                '}';
    }

}
