package lab11.ex1;

public class TemperatureController {
    public TemperatureController(){
        Thermometer t=new Thermometer();
        TemperatureTextView ttv=new TemperatureTextView();
        t.addObserver(ttv);
        t.start();
    }

    public static void main(String[] args) {
        TemperatureController t=new TemperatureController();
    }
}
