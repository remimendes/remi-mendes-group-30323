package lab6.ex4;

import java.util.HashMap;
import java.util.Iterator;

public class Dictionary {
    HashMap<Word, Definition> map = new HashMap<>();

    public Dictionary() {
    }

    public void addWord(Word w, Definition d) {
        map.put(w, d);
    }

    public Definition getDefinition(Word w) {
        Iterator it = map.keySet().iterator();
        Word tmpWord;
        while (it.hasNext()) {
            tmpWord = (Word)it.next();
            if( tmpWord.getName().equals(w.getName())){
                return map.get(tmpWord);
            };

        }

         return null;
    }

    public void getAllWords() {
        for (Word w : map.keySet()) {
            System.out.println(w);
        }
    }

    public void getAllDefinition() {
        for (Definition d : map.values()) {
            System.out.println(d);
        }
    }
}
