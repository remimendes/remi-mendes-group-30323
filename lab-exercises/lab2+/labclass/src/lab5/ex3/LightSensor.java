package lab5.ex3;

import java.util.Random;

public class LightSensor extends Sensor{
    public LightSensor(){};
    @Override
    public int readValue() {
        Random r = new Random();
        return r.nextInt(101);
    }
}
