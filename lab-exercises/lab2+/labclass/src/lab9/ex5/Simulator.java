package lab9.ex5;

import java.util.ArrayList;

import static java.lang.Thread.currentThread;
import static java.lang.Thread.sleep;

public class Simulator {

    /**
     * @param args
     */
    public static void main(String[] args) {
        ArrayList<Controler> StationList=new ArrayList<>();
        Frame f=new Frame(1200,1200,3,StationList);
        //build station Cluj-Napoca
        Controler c1 = new Controler("Cluj-Napoca",f,f.getFreeTextArea());
        StationList.add(c1);

        Segment s1 = new Segment(1);
        Segment s2 = new Segment(2);
        Segment s3 = new Segment(3);

        c1.addControlledSegment(s1);
        c1.addControlledSegment(s2);
        c1.addControlledSegment(s3);

        //build station Bucuresti
        Controler c2 = new Controler("Bucuresti",f, f.getFreeTextArea());
        StationList.add(c2);

        Segment s4 = new Segment(4);
        Segment s5 = new Segment(5);
        Segment s6 = new Segment(6);

        c2.addControlledSegment(s4);
        c2.addControlledSegment(s5);
        c2.addControlledSegment(s6);
        //build station Chantilly-Gouvieux

        Controler c3 = new Controler("Chantilly-Gouvieux",f,f.getFreeTextArea());
        StationList.add(c3);

        Segment s7 = new Segment(7);
        Segment s8 = new Segment(8);
        Segment s9 = new Segment(9);

        c3.addControlledSegment(s7);
        c3.addControlledSegment(s8);
        c3.addControlledSegment(s9);
        //connect the 3 controllers

        c1.addNeighbourController(c2);
        c1.addNeighbourController(c3);

        c2.addNeighbourController(c1);
        c2.addNeighbourController(c3);

        c3.addNeighbourController(c1);
        c3.addNeighbourController(c2);

        //testing

        Train t1 = new Train("Bucuresti", "IC-001");
        s1.arriveTrain(t1);

        Train t2 = new Train("Cluj-Napoca","R-002");
        s5.arriveTrain(t2);

        Train t3 = new Train("Bucuresti","R-004");
        s8.arriveTrain(t3);

        c1.displayStationState();
        c2.displayStationState();
        c3.displayStationState();

        while (!f.isNext()){
            try {
                sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        f.setNext(false);
        f.clearText();
        //execute 3 times controller steps
        for(int i = 0;i<3;i++){
            c1.clearTextAreaC();
            c2.clearTextAreaC();
            c3.clearTextAreaC();
            f.clearText();
            f.addText("### Step "+i+" ###");
            c1.controlStep();
            c2.controlStep();
            c3.controlStep();

            System.out.println();
            f.setNext(false);
            f.clearText();

            c1.displayStationState();
            c2.displayStationState();
            c3.displayStationState();
            while (!f.isNext()){
                try {
                    sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            f.setNext(false);
        }

    }

}