package lab2;

import java.util.Scanner;

public class Ex3 {
    public static void main(String[] args){
        System.out.print("enter two integer :");
        Scanner in = new Scanner(System.in);
        int a= in.nextInt();
        int b= in.nextInt();
        int max=a>=b?a:b;
        int min=a>=b?b:a;
        int list[]= new int[max];
        int index=0;
        int nbr=0;
        boolean bo;
        for(int i=2;i<max;i++) {
            bo = false;
            for (int j=0;j<index;j++) {
                if (i % list[j] == 0) {
                    bo = true;
                }
            }
            if (bo == false) {
                list[index] = i;
                index++;
            }
        }
        for (int j=0;j<index;j++){
            if (min<list[j]&&max>list[j]){
                System.out.print(list[j]+" ");
                nbr++;
            }
        }
        System.out.print("there are/is "+nbr+" prime numbers between "+min+" and "+max);
    }
}
