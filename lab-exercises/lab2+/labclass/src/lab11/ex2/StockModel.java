package lab11.ex2;

import java.util.HashMap;
import java.util.Set;

public class StockModel {
    private HashMap<String,Stock> StocksData=new HashMap<>();
    public StockModel(){};

    public void addProduct(String name,float price,int quantity){
        StocksData.put(name,new Stock(price,quantity));
    }

    public void deleteProduct(String name){
        StocksData.remove(name);
    }

    public float getPrice(String name){
        return StocksData.get(name).getPrice();
    }

    public int getQuantity(String name){
        return StocksData.get(name).getQuantity();
    }

    public void setPrice(String name,float price){
        StocksData.get(name).setPrice(price);
    }

    public void setQuantity(String name,int quantity){
        StocksData.get(name).setQuantity(quantity);
    }

    public Set getAllKeys() {
        return StocksData.keySet();
    }

    public Stock getStock(String name){
        return StocksData.get(name);
    }
}
