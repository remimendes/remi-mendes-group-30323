package lab6.ex1;

import java.util.Objects;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String owner,double balance){
        this.owner=owner;
        this.balance=balance;
    }

    public void deposit(double amount){
        balance+=amount;
    }

    public void withdraw(double amount){
        balance-=amount;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof BankAccount){
            BankAccount b = (BankAccount)o;
            return b.balance==this.balance && b.owner.equals(this.owner);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return (int)balance+owner.hashCode();
    }


    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("Mathieu Herve",2000);
        BankAccount b2 = new BankAccount("remi Mendes",8);
        BankAccount b3 = new BankAccount("remi Mendes",8);
        System.out.println(b2.equals(b3));
        System.out.println(b2.hashCode());
        System.out.println(b3.hashCode());
        System.out.println(b1.hashCode());
    }
}
