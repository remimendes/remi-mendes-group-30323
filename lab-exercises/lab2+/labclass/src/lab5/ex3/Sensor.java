package lab5.ex3;

import java.util.Random;

public abstract class Sensor {
    private String location;

    public abstract int readValue();

    public String getLocation() {
        return location;
    }
}
