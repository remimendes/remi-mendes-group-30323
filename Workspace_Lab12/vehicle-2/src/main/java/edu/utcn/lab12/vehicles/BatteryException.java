package edu.utcn.lab12.vehicles;

public class BatteryException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BatteryException(final String msg){
        super(msg);
    }
	
	public BatteryException(final String msg, final Throwable cause){
        super(msg, cause);
    }

}
