package lab4;
import lab3.Ex1.Author;

public class BookBis {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock=0;

    public BookBis(String name,Author[] authors,double price){
        this.name=name;
        this.authors=authors;
        this.price=price;
    }

    public BookBis(String name,Author[] authors,double price, int qtyInStock){
        this.name=name;
        this.authors=authors;
        this.price=price;
        this.qtyInStock=qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return name+ " by " + authors.length + " author(s)";
    }
    public void printAuthors(){
        for(int i=0;i< authors.length-1;i++){
            System.out.println("author "+i+" : "+authors[i]);
        }
    }
    public static void main(String[] args) {
        Author author1 =new Author("remi","remi.mendes@yahoo.com",'m');
        Author author2= new Author("mathieu","mat@gmail.com",'m');
        Author[] authorsList = {author1,author2};

        BookBis book1 = new BookBis("Les fleurs du mal",authorsList,10);
        System.out.println(book1);
    }
}
