package lab3.Ex1;

public class Flower {
    private int petal;
    private static int numberOfFlower=0;

    public Flower() {
        System.out.println("Flower has been created!");
        numberOfFlower+=1;
    }

    public static int getNumberOfFlower() {
        return numberOfFlower;
    }

    public static void main(String[] args) {
        Flower f1 = new Flower();
        Flower f2 = new Flower();
        Flower f3 = new Flower();
        System.out.println(f1.getNumberOfFlower());
    }
}

