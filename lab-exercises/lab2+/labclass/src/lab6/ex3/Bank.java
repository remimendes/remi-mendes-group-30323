package lab6.ex3;

import java.util.*;

public class Bank {
    Comparator<lab6.ex3.BankAccount> C = new Comparator<lab6.ex3.BankAccount>() {
        @Override
        public int compare(lab6.ex3.BankAccount b1, lab6.ex3.BankAccount b2) {
            if (b1.getBalance() > b2.getBalance()) {
                return -1;
            } else if (b1.getBalance() < b2.getBalance()) {
                return 1;
            } else {
                return 0;
            }
        }
    };
    private TreeSet<lab6.ex3.BankAccount> bankAccountTreeSet = new TreeSet<lab6.ex3.BankAccount>(C);

    public Bank() {
    }

    ;

    public void addAccount(String owner, double balance) {
        bankAccountTreeSet.add(new lab6.ex3.BankAccount(owner, balance));
    }

    public void printAccounts() {
        Iterator iterator = bankAccountTreeSet.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        Iterator it = bankAccountTreeSet.iterator();
        lab6.ex3.BankAccount tmpB;

        while (it.hasNext()) {
            tmpB=(lab6.ex3.BankAccount)it.next();
            if (tmpB.getBalance()>=minBalance && tmpB.getBalance()<=maxBalance){
            System.out.println(tmpB);
        }
    }
    }

    public lab6.ex3.BankAccount getAccount(String owner) {
        Iterator it = bankAccountTreeSet.iterator();
        lab6.ex3.BankAccount tmpBankAccount= (lab6.ex3.BankAccount)it.next();
        if (tmpBankAccount.getOwner().equals(owner)){
            return tmpBankAccount;
        }
        while (it.hasNext()){
            tmpBankAccount= (lab6.ex3.BankAccount) it.next();
            if(tmpBankAccount.getOwner().equals(owner)){
                break;
            }
        }
        return tmpBankAccount;
    }

    public TreeSet<lab6.ex3.BankAccount> getAllAccounts() {
        Comparator<lab6.ex3.BankAccount> C = new Comparator<lab6.ex3.BankAccount>() {
            @Override
            public int compare(BankAccount o1, BankAccount o2) {
                return o1.getOwner().compareTo(o2.getOwner());
            }
        };
        TreeSet bankAccountTreeSetSortedByOwner = new TreeSet<lab6.ex3.BankAccount>(C);
        Iterator it = bankAccountTreeSet.iterator();

        while(it.hasNext()) {
            bankAccountTreeSetSortedByOwner.add( it.next());

        }
        return bankAccountTreeSetSortedByOwner;
    }


    public static void main(String[] args) {

        Bank b = new Bank();
        b.addAccount("remi", 200);
        b.addAccount("mat", 2);
        b.addAccount("vagl", 400);
        b.addAccount("aggaa", 377);
        b.addAccount("val", 999);
        b.addAccount("mdr", 300);

        TreeSet tmp = b.getAllAccounts();
        Iterator it = tmp.iterator();
        while (it.hasNext()){
            System.out.println(it.next());
        }
    }
}

