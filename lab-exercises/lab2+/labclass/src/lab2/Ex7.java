package lab2;

import java.util.Random;
import java.util.Scanner;


public class Ex7 {
    public static void main(String[] args){
        Random r=new Random();
        Scanner in =new Scanner(System.in);
        int nbr=r.nextInt(11);
        int life=3;
        int guess;
        do{
            System.out.print("guess the number between 0 and 10 (included)-->");
            guess=in.nextInt();
            if (guess>nbr){
                System.out.print("too high \n");
                life--;
            }
            else if (guess<nbr){
                System.out.print("too low \n");
                life--;
            }
            else {
                break;
            }
        }while(life>0);
        if (life==0){
            System.out.print("game over \n");
            System.out.print("the number was :"+nbr);
        }
        else {
            System.out.print("you won");
        }
    }
}
