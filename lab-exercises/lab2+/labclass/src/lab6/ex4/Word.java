package lab6.ex4;

import java.util.Objects;

public class Word {
    private String name;
    public Word(String name){
        this.name=name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Word{" +
                "name='" + name + '\'' +
                '}';
    }
}
