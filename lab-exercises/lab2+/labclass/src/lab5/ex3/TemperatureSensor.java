package lab5.ex3;

import java.util.Random;

public class TemperatureSensor extends Sensor{
    public TemperatureSensor(){};
    @Override
    public int readValue() {
        Random r = new Random();
        return r.nextInt(101);
    }
}
