package edu.utcn.lab12.devices;

import static org.junit.Assert.*;

public class ElectronicDeviceTest {
	public static void main(String[] args) {
		
		ElectronicDevice ed = new ElectronicDevice();
		
		assertTrue(ed.isPowered() == true);
		ed.turnOn();
		assertTrue(ed.isPowered() == true);
		ed.turnOff();
		assertTrue(ed.isPowered() == false);
	}
}
