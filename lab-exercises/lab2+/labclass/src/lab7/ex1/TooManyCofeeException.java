package lab7.ex1;

public class TooManyCofeeException extends Exception{
    private int nbOfCofee;
    public TooManyCofeeException(int nbOfCofee,String msg) {
        super(msg);
        this.nbOfCofee = nbOfCofee;
    }

    int getNbOfCofee(){
        return nbOfCofee;
    }
}
