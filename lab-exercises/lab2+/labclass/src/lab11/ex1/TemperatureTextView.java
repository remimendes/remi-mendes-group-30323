package lab11.ex1;

import java.awt.*;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;

public class TemperatureTextView extends JFrame implements Observer{

    JLabel jtlTemp;

    TemperatureTextView(){
        this.setLayout(new GridLayout());
        jtlTemp = new JLabel("Temperature");
        add(jtlTemp);
        setSize(200,200);
        setVisible(true);
    }

    public void update(Observable o, Object arg) {
        String s = "Temperature :"+((Thermometer)o).getTemperature();
        jtlTemp.setText(s);
    }

}