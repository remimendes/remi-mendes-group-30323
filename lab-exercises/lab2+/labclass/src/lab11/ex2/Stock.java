package lab11.ex2;

public class Stock {
    private float price;
    private int quantity;

    public Stock(float price,int quantity){
        this.price=price;
        this.quantity=quantity;
    }

    public float getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
