package lab10.ex3;

public class Counter extends Thread{
    static int count=0;
    Thread thread;
    public Counter(String name,Thread thread){
        super(name);
        this.thread=thread;
    }
    @Override
    public void run() {
        if (thread!=null){
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        count();
    }

    public void count(){
        for (int i = 0; i < 100; i++) {
            count++;
            System.out.println(getName()+"--count : "+count );
        }
    }

    public static void main(String[] args) {
        Counter c1=new Counter("counter 1",null);
        Counter c2=new Counter("counter 2",c1);

        c1.start();
        c2.start();
    }
}
